import os
import torch
import numpy as np
import pandas as pd
import torch.nn.functional as F

from skimage import io, transform
from torch.utils.data import Dataset, ConcatDataset


DATA_MODES = ['train', 'test']


class ColorConstancyDataset(Dataset):
    def __init__(
        self,
        csv_file: str,
        root_dir: str,
        mode='train',
        transform=None
    ) -> dict:
        """
        Create dataset for color constancy image data.
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            mode (string): Train or test dataset to be created.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        if mode not in DATA_MODES:
            raise ValueError(
                f"Mode {self.mode} is not correct; correct modes: {DATA_MODES}"
            )

        self.gt_csv = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform
        self.mode = mode

    def __len__(self):
        return len(self.gt_csv)

    def __getitem__(self, idx):
        img_path = os.path.join(self.root_dir, self.gt_csv.iloc[idx, 0])
        image = io.imread(img_path + '.png')
        image_name = os.path.basename(img_path)
        light_source = self.gt_csv.iloc[idx, 1:]
        sample = {
            'image_name': image_name,
            'image': image,
            'light_source': light_source.to_numpy(dtype=np.float64)
        }

        if self.transform:
            sample = self.transform(sample)

        if self.mode == 'test':
            return {
                'image_name': image_name,
                'image': sample['image'],
                'light_source': np.array(())
            }

        return sample


class DADataset(Dataset):
    """DADataset dataset."""

    def __init__(
        self,
        src_dataset,
        trg_dataset=None,
        trg_dataset_unlabeled=None,
        ratio=[0, 0]
    ):
        """
        Args:
            src_dataset (Dataset):
            trg_dataset (Dataset):
            trg_dataset_unlabeled (Dataset):
        """
        self.src_dataset = src_dataset
        self.len_src = len(self.src_dataset)

        if trg_dataset is not None:
            self.trg_dataset = self.clone_dataset(trg_dataset, ratio[0])
        else:
            self.trg_dataset = None
        self.len_trg = int(np.ceil(self.len_src * ratio[0]))

        if trg_dataset_unlabeled is not None:
            self.trg_dataset_unlabeled = self.clone_dataset(
                trg_dataset_unlabeled, ratio[1]
            )
        else:
            self.trg_dataset_unlabeled = None
        self.len_trg_unlabeled = int(np.ceil(self.len_src * ratio[1]))

    def clone_dataset(self, dataset, ratio):
        n = int(np.ceil(self.len_src / len(dataset) * ratio))
        return ConcatDataset([dataset for _ in range(n)])

    def __len__(self):
        return self.len_src + self.len_trg + self.len_trg_unlabeled

    def __getitem__(self, idx):
        if idx < self.len_src:
            return self.src_dataset[idx] | {'class': 0., 'reg_mask': 1}
        if idx < self.len_src + self.len_trg:
            return self.trg_dataset[idx - self.len_src] | {'class': 1., 'reg_mask': 1}

        result = (
            self.trg_dataset_unlabeled[idx - (self.len_src + self.len_trg)] |
            {'class': 1., 'reg_mask': 0}
        )
        return result


class CubePPDataset(Dataset):
    """CubePlusPlus dataset."""

    def __init__(self, csv_file, root_dir, transform=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.images_frame = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform

    def __len__(self):
        return len(self.images_frame)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        img_name = os.path.join(self.root_dir,
                                self.images_frame.iloc[idx, 0])
        image = io.imread(img_name + '.png')
        # single prediction value on mean of sides of cube
        light_source = self.images_frame.iloc[idx, 1:4]

        sample = {'image': image,
                  'light_source': light_source.to_numpy(dtype=np.float64)}

        if self.transform:
            sample = self.transform(sample)

        return sample


class ColorCheckerDataset(Dataset):

    def __init__(self, csv_file, root_dir, transform=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.images_frame = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform

    def __len__(self):
        return len(self.images_frame)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        img_name = os.path.join(self.root_dir,
                                self.images_frame.iloc[idx, 0])

        image = io.imread(img_name + '.png')
        light_source = self.images_frame.iloc[idx, 1:]

        sample = {'image': image,
                  'light_source': light_source.to_numpy(dtype=np.float64)}

        if self.transform:
            sample = self.transform(sample)

        return sample


class SimpleCubePPDataset(Dataset):
    """Simple CubePlusPlus dataset."""

    def __init__(self, csv_file, root_dir, transform=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.images_frame = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform

    def __len__(self):
        return len(self.images_frame)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        img_name = os.path.join(self.root_dir,
                                self.images_frame.iloc[idx, 0])
        image = io.imread(img_name + '.png')
        light_source = self.images_frame.iloc[idx, 1:]

        sample = {'image': image,
                  'light_source': light_source.to_numpy(dtype=np.float64)}

        if self.transform:
            sample = self.transform(sample)

        return sample


def rgb2chrom(r, g, b):
    y = (2 * b - (r + g)) / (r + g + b)
    x = np.sqrt(3) * (r - g) / (r + g + b)

    return np.array([x, y])


class SimpleCubePPDatasetChroma(Dataset):
    """Simple CubePlusPlus dataset."""

    def __init__(self, csv_file, root_dir, transform=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.images_frame = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform

    def __len__(self):
        return len(self.images_frame)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        img_name = os.path.join(self.root_dir,
                                self.images_frame.iloc[idx, 0])
        image = io.imread(img_name + '.png')
        light_source = self.images_frame.iloc[idx, 1:]
        ls = light_source.to_numpy(dtype=np.float64)

        sample = {'image': image, 'light_source': rgb2chrom(ls[0], ls[1], ls[2])}

        if self.transform:
            sample = self.transform(sample)

        return sample


class Rescale(object):
    """Rescale the image in a sample to a given size.

    Args:
        output_size (tuple or int): Desired output size. If tuple, output is
            matched to output_size. If int, smaller of image edges is matched
            to output_size keeping aspect ratio the same.
    """

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        self.output_size = output_size

    def __call__(self, sample):
        image_name = sample['image_name']
        image = sample['image']
        light_source = sample['light_source']

        h, w = image.shape[:2]
        if isinstance(self.output_size, int):
            if h > w:
                new_h, new_w = self.output_size * h / w, self.output_size
            else:
                new_h, new_w = self.output_size, self.output_size * w / h
        else:
            new_h, new_w = self.output_size

        new_h, new_w = int(new_h), int(new_w)
        image = transform.resize(image, (new_h, new_w))

        # x and y axes are axis 1 and 0 respectively
        return {
            'image_name': image_name,
            'image': image,
            'light_source': light_source
        }


class RandomCrop(object):
    """Crop randomly the image in a sample.

    Args:
        output_size (tuple or int): Desired output size. If int, square crop
            is made.
    """

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        if isinstance(output_size, int):
            self.output_size = (output_size, output_size)
        else:
            assert len(output_size) == 2
            self.output_size = output_size

    def __call__(self, sample):
        image_name = sample['image_name']
        image = sample['image']
        light_source = sample['light_source']

        h, w = image.shape[:2]
        new_h, new_w = self.output_size

        top = np.random.randint(0, h - new_h)
        left = np.random.randint(0, w - new_w)

        image = image[top: top + new_h,
                      left: left + new_w]

        light_source = light_source - [left, top]

        return {
            'image_name': image_name,
            'image': image,
            'light_source': light_source
        }


class RandomHorizontalFlip(object):
    def __init__(self, p=0.5):
        assert isinstance(p, int)
        self.p = p

    def forward(self, sample):
        image_name = sample['image_name']
        image = sample['image']
        light_source = sample['light_source']

        if torch.rand(1) < self.p:
            image = F.hflip(image)
            return {
                'image_name': image_name,
                'image': image,
                'light_source': light_source
            }

        return {
            'image_name': image_name,
            'image': image,
            'light_source': light_source
        }


class RandomVerticalFlip(object):
    def __init__(self, p=0.5):
        assert isinstance(p, int)
        self.p = p

    def forward(self, sample):
        image_name = sample['image_name']
        image = sample['image']
        light_source = sample['light_source']

        if torch.rand(1) < self.p:
            image = F.vflip(image)
            return {
                'image_name': image_name,
                'image': image,
                'light_source': light_source
            }

        return {
            'image_name': image_name,
            'image': image,
            'light_source': light_source
        }


class ToTensor(object):
    """Convert ndarrays in sample to Tensors."""

    def __call__(self, sample):
        image_name = sample['image_name']
        image = sample['image']
        light_source = sample['light_source']

        # swap color axis because
        # numpy image: H x W x C
        # torch image: C x H x W
        image = image.transpose((2, 0, 1))
        return {
            'image_name': image_name,
            'image': torch.from_numpy(image),
            'light_source': torch.from_numpy(light_source)
        }
