import os
import cv2
import click
import numpy as np
import pandas as pd


def image_correction(
        raw_img: np.array,
        ie: np.array
) -> np.array:
    '''
    This function makes an image correction using given illuminant estimation.

    Args:
        img (np.array): Raw image to be corrected in RGB format:
        ie (np.array): Estimation of source illuminant on the image.

    Return:
        corrected_img (np.array): Image with correction of the light source.
    '''

    exposure_boost = 5
    raw_img = raw_img.astype('float32') / np.iinfo(raw_img.dtype).max * exposure_boost

    # Calculate scaling factors for the channels
    scale_factors = ie / np.mean(ie)

    # Apply the scale to each channel
    corrected_img = raw_img.copy()
    for i in range(3):
        corrected_img[:, :, i] = raw_img[:, :, i] / scale_factors[i]

    # Clip values to the valid range for an image [0,...,1]
    corrected_img = np.clip(corrected_img, 0, 1)

    # Exposure correction
    gamma = 2.2
    corrected_img = np.power(corrected_img, 1 / gamma)

    # Convert the corrected image back to the original data type
    corrected_img = (corrected_img * 255).astype(np.uint8)

    return corrected_img


@click.command()
@click.argument('img_path', type=click.Path(exists=True))
@click.argument('gt_path', type=click.Path(exists=True))
@click.argument('output_path', type=click.Path(exists=True))
@click.argument('img_type', type=click.STRING)
def main(
    img_path: str,
    gt_path: str,
    output_path: str,
    img_type: str
) -> None:
    img_name = os.path.basename(img_path).split('.')[0]
    raw_img = cv2.imread(img_path, cv2.IMREAD_UNCHANGED)

    if raw_img is None:
        return RuntimeError(f"Image not found: {img_path}")

    gt_df = pd.read_csv(gt_path)
    print(gt_df[gt_df['image'] == img_name])
    _, mean_r, mean_g, mean_b = gt_df[gt_df['image'] == img_name].values[0]

    illuminant_estimation = np.array([mean_r, mean_g, mean_b])
    corrected_img = image_correction(raw_img, illuminant_estimation)

    cv2.imwrite(f'{output_path}/{img_name}_{img_type}.png', corrected_img)


if __name__ == '__main__':
    main()
