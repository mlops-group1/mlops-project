import os
import torch
import click
import pandas as pd
import numpy as np

from torch.utils.data import DataLoader
from torchvision import transforms
from tqdm import tqdm

from src.models.nets import ViTEncoder, FullColorCerberusDA
from src.data.dataset import ColorConstancyDataset
from src.data.dataset import Rescale, ToTensor


BATCH_SIZE = 8
NUM_WORKERS = 2


@click.command()
@click.argument('model_weights_path', type=click.STRING)
@click.argument('architecture', type=click.STRING)
@click.argument('output_dir_path', type=click.STRING)
@click.argument('dataset_dir_path', type=click.STRING)
def evaluate(
    model_weights_path: str,
    architecture: str,
    output_dir_path: str,
    dataset_dir_path: str
) -> None:
    """Evaluate the model with a given weights on the dataset and save the results.

    Args:
        model_weights_path (str): Name of the run.
        architecture (str): Type of the encoder part of the model.
        output_dir_path (str): The directory where the outputs.csv file will be saved.
        dataset_dir_path (str): The directory where the images to be evaluated
            are storedand .csv file with names of the images.
    """

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Load the desired model
    if architecture == 'color_cerberus':
        model = FullColorCerberusDA()
        image_scale = (128, 192)
    elif architecture == 'vit':
        model = ViTEncoder()
        image_scale = (224, 224)
    else:
        raise ValueError(
            f"There is no implementation of such architecture: {architecture}.\
            check the src/models/nets.py"
        )

    model.load_state_dict(torch.load(model_weights_path, map_location=device))
    model = model.to(device)
    model.eval()

    # Load the dataset
    dataset = ColorConstancyDataset(
        root_dir=dataset_dir_path + '/PNG',
        csv_file=dataset_dir_path + '/gt.csv',
        transform=transforms.Compose([
            Rescale(image_scale),
            ToTensor()
        ])
    )
    dataloader = DataLoader(
        dataset=dataset,
        batch_size=BATCH_SIZE,
        shuffle=True,
        num_workers=NUM_WORKERS,
        drop_last=False
    )

    # Run the model on the dataset
    data = []
    with torch.no_grad():
        for batch in tqdm(dataloader, desc='Validation'):
            X_batch = batch['image'].to(device)
            y_pred_rgb, _ = model(X_batch, 0)

            # Concatenate image names with predictions
            batch_data = np.column_stack((batch['image_name'], y_pred_rgb))
            data.extend(batch_data)

    # Convert the data to DataFrame and save results to .csv file
    pred_df = pd.DataFrame(data, columns=['image', 'mean_r', 'mean_g', 'mean_b'])

    output_type = dataset_dir_path.strip('/').split('/')[-1]  # train or test
    output_filename = 'outputs_' + output_type + '.csv'
    output_path = os.path.join(output_dir_path, output_filename)
    pred_df.to_csv(output_path, index=False)


if __name__ == "__main__":
    evaluate()
