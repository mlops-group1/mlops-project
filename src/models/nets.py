import torch
from torch import nn
from torch.autograd import Function
from transformers import ViTModel


class ReverseLayerF(Function):
    @staticmethod
    def forward(ctx, x, alpha):
        ctx.alpha = alpha

        return x

    @staticmethod
    def backward(ctx, grad_output):
        output = -grad_output * ctx.alpha
        return output, None


class FullColorCerberusDA(nn.Module):
    def __init__(self):
        super().__init__()
        self.avgpool = nn.AvgPool2d((128, 192))
        self.ups = nn.Upsample((128, 192))

        self.conv_part = nn.Sequential(
            nn.Conv2d(6, 16, 3, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(2),

            nn.Conv2d(16, 32, 3, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(2),

            nn.Conv2d(32, 64, 3, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(2),

            nn.Conv2d(64, 128, 3, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(2),

            nn.Conv2d(128, 16, 1),
            nn.ReLU(),
            nn.Flatten()
        )

        self.linear_part = nn.Sequential(
            nn.Linear(1536, 400),
            nn.ReLU(),
            nn.Linear(400, 3)
        )

        self.domain_classifier = nn.Sequential(
            nn.Linear(1536, 400),
            nn.ReLU(),
            nn.Linear(400, 1),
        )

    def forward(self, x, alpha):
        x = x.float()
        output = self.avgpool(x)
        output = self.ups(output)
        output = torch.cat((output, x), 1)

        feature = self.conv_part(output)
        reverse_feature = ReverseLayerF.apply(feature, alpha)

        output = self.linear_part(feature)
        domain_output = self.domain_classifier(reverse_feature)

        return output.double(), domain_output.double()


class ViTEncoder(nn.Module):
    def __init__(self, model_checkpoint='google/vit-base-patch16-224-in21k'):
        super().__init__()

        self.vit = ViTModel.from_pretrained(model_checkpoint, add_pooling_layer=False)
        self.linear_part = (
            nn.Linear(768, 3)
        )
        self.domain_classifier = (
            nn.Linear(768, 1)
        )

    def forward(self, x, alpha):
        output = self.vit(x)['last_hidden_state']

        feature = output[:, 0, :]  # Use the embedding of [CLS] token
        reverse_feature = ReverseLayerF.apply(feature, alpha)

        output = self.linear_part(feature)
        domain_output = self.domain_classifier(reverse_feature)

        return output.double(), domain_output.double()
