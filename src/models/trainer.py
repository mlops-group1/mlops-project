import time
import wandb

from collections import defaultdict
from typing import Any, Callable, Dict, Optional

import numpy as np
import torch
from torch.utils.data import DataLoader
from tqdm import tqdm


class Trainer:
    def __init__(
        self,
        train_dataloader: DataLoader,
        test_dataloader: DataLoader,
        loss: type,
        model_factory: Callable,
        optimizer_factory: Callable,
        scheduler_factory: Callable,
        model_params: Dict[str, Any],
        optimizer_params: Dict[str, Any],
        scheduler_params: Dict[str, Any],
        other_params: Dict[str, Any],
        log: Optional[bool] = False,
        wandb_init_params: Optional[Dict[str, str]] = None,
        desc: Optional[str] = None,
        model_dir: Optional[str] = None,
        forward_params: Optional[Dict[str, Any]] = {}
    ):
        """
            Class for entire model training and validation process.
            It implements an initialization of the model, optimizer and scheduler.
            It connents with wandb project and logs results.

            An example of usage can be found in usage_example.ipynb

            Argument 'model_params' should contain 'device' key.
        """
        self.train_dataloader = train_dataloader
        self.test_dataloader = test_dataloader
        self.loss = loss

        # initializing
        self.model = model_factory(**model_params).to(model_params['device'])
        self.optimizer = optimizer_factory(self.model.parameters(), **optimizer_params)
        self.scheduler = scheduler_factory(self.optimizer, **scheduler_params)

        self.model_params = model_params
        self.optimizer_params = optimizer_params
        self.scheduler_params = scheduler_params
        self.other_params = other_params
        self.forward_params = forward_params

        self.best_decision_metric = np.inf
        self.best_epoch = None

        # creating a new experiment and log hyperparams
        self.log = log
        self.desc = desc if desc is not None else ''
        self.model_dir = model_dir if model_dir is not None else './'

        self.wandb_init_params = wandb_init_params
        if self.wandb_init_params is None:
            self.wandb_init_params = {
                'name': 'empty_name'
            }
        self.run = None
        if self.log:
            self.run = self.log_hyperparams(
                self.wandb_init_params,
                **self.model_params,
                **self.optimizer_params,
                **self.scheduler_params,
                **self.other_params,
                **self.forward_params
            )
        # initializing metrics
        self.metrics = None
        self.init_metrics()

    @staticmethod
    def log_hyperparams(
        wandb_init_params: Dict[str, str],
        **hyperparams
    ):
        run = wandb.init(**wandb_init_params, config={})
        wandb.config.update(hyperparams)
        return run

    @staticmethod
    def log_metrics(metrics: Dict[str, Any]):
        wandb.log(metrics)

    def log_training_step_artifacts(self, epoch):
        if 'name' in self.wandb_init_params:
            name = self.wandb_init_params['name']
        else:
            print(
                """
                    ERROR -- Function 'wandb_training_step_artifacts':
                    The key name is not in the dictionary 'wandb_init_params'.
                """
            )
            raise KeyError

        model_artifact = wandb.Artifact(
            name + f'_{epoch}',
            type="model",
            description=f"{self.desc}"
        )

        log_path = self.model_dir + name + ".pth"
        torch.save(self.model.state_dict(), log_path)
        model_artifact.add_file(log_path)
        wandb.save(log_path)

        self.run.log_artifact(model_artifact)

    def calc_metric_value(self, epoch, val_to_update):
        decision_metric = 'test_' + self.other_params['decision_metric']
        if self.best_decision_metric > val_to_update[decision_metric]:
            self.best_decision_metric = val_to_update[decision_metric]
            self.best_epoch = epoch

        return {
            'best_epoch': self.best_epoch,
            'best_metric': self.best_decision_metric
        }

    def init_metrics(self):
        self.metrics = defaultdict(list)

    def update_metrics(self, **to_update):
        if self.log:
            self.log_metrics(to_update)
        for name, value in to_update.items():
            self.metrics[name].append(value)

    def train_model(self, learning_params: Dict[str, Any]):
        """
            Argument 'learning_params' should contain 'num_epoch' key.
        """
        if 'num_epoch' not in learning_params:
            print(
                """
                    ERROR -- Function train_model:
                    The key 'num_epoch' is not in the dictionary 'learning_params'.
                """
            )
            raise KeyError

        num_epoch = learning_params['num_epoch']
        for epoch in range(num_epoch):
            start_time = time.time()

            train_to_update = self.train_epoch(learning_params)
            val_to_update = self.validate_epoch()

            if self.log:
                self.log_training_step_artifacts(epoch)
            best_epoch_to_update = self.calc_metric_value(epoch, val_to_update)
            self.update_metrics(
                epoch=epoch,
                **train_to_update,
                **val_to_update,
                **best_epoch_to_update,
                lr=self.scheduler.get_last_lr()[0]
            )

            print("Epoch: {} of {}, {:.3f} min".format(
                epoch + 1, num_epoch, (time.time() - start_time) / 60))

    def train_epoch(
        self,
        learning_params: Dict[str, Any],
        dataloader: Optional[DataLoader] = None,
        log_prefix: Optional[str] = 'train'
    ):
        dataloader = dataloader if dataloader is not None else self.train_dataloader
        model = self.model
        device = model.device
        opt = self.optimizer
        scheduler = self.scheduler
        scaler = torch.cuda.amp.GradScaler(enabled=learning_params['use_amp'])

        metrics = defaultdict(list)
        model.train(True)
        for batch in tqdm(dataloader, desc='Training'):
            opt.zero_grad()

            X_batch = batch['image'].to(device)
            y_rgb_batch = batch['light_source'].to(device)
            y_cls_batch = batch['class'].to(device)
            reg_mask_batch = batch['reg_mask'].to(device)
            with torch.autocast(
                device_type='cuda',
                dtype=torch.float16,
                enabled=learning_params['use_amp']
            ):

                y_pred_rgb, y_pred_cls = model(X_batch, **self.forward_params)
                loss, cur_metrics = self.loss(
                    y_pred_rgb, y_pred_cls, y_rgb_batch, y_cls_batch, reg_mask_batch
                )

            scaler.scale(loss).backward()
            scaler.step(opt)
            scaler.update()

            for name, value in cur_metrics.items():
                metrics[name].append(value)

        scheduler.step()
        to_update = {
            log_prefix + '_' + name: np.mean(values) for name, values in metrics.items()
        }
        return to_update

    def validate_epoch(
        self,
        dataloader: Optional[DataLoader] = None,
        log_prefix: Optional[str] = 'test'
    ):
        dataloader = dataloader if dataloader is not None else self.test_dataloader
        model = self.model
        device = model.device

        metrics = defaultdict(list)

        model.eval()
        with torch.no_grad():
            for batch in tqdm(dataloader, desc='Validation'):
                X_batch = batch['image'].to(device)
                y_rgb_batch = batch['light_source'].to(device)
                y_cls_batch = batch['class'].to(device)
                reg_mask_batch = batch['reg_mask'].to(device)

                y_pred_rgb, y_pred_cls = model(X_batch, **self.forward_params)
                loss, cur_metrics = self.loss(
                    y_pred_rgb, y_pred_cls, y_rgb_batch, y_cls_batch, reg_mask_batch)

                for name, value in cur_metrics.items():
                    metrics[name].append(value)

        to_update = {
            log_prefix + '_' + name: np.mean(values) for name, values in metrics.items()
        }
        return to_update
