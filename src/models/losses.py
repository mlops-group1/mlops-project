import numpy as np
import torch.nn as nn


def calc_angular_error(a, b):
    a_norm = a / np.linalg.norm(a, axis=1, keepdims=True)
    b_norm = b / np.linalg.norm(b, axis=1, keepdims=True)

    return np.mean(
        np.degrees(np.arccos(np.clip(np.sum(a_norm * b_norm, axis=1), -1.0, 1.0)))
    )


class DALoss:
    def __init__(self, domain_ratio=0.5):
        self.domain_ratio = domain_ratio
        self.domain_loss = nn.BCEWithLogitsLoss()
        self.rgb_loss = nn.MSELoss()

    def __call__(self, y_pred_rgb, y_pred_cls, y_true_rgb, y_true_cls, mask):
        rgb_loss = self.rgb_loss(
            y_pred_rgb[mask.bool()], y_true_rgb[mask.bool()]
        )
        domain_loss = self.domain_loss(y_pred_cls.ravel(), y_true_cls)

        loss = self.domain_ratio * domain_loss + (1 - self.domain_ratio) * rgb_loss
        domain_accuracy = (y_pred_cls.detach().cpu().argmax(dim=1) ==
                           y_true_cls.detach().cpu()).float().mean()

        metrics = {
            'loss': loss.item(),
            'rgb_loss': rgb_loss.item(),
            'domain_loss': domain_loss.item(),
            'domain_accuracy': domain_accuracy.item()
        }

        if (y_true_cls == 0).any():
            metrics['angular_error_s'] = calc_angular_error(
                y_pred_rgb[y_true_cls == 0].detach().cpu().numpy(),
                y_true_rgb[y_true_cls == 0].detach().cpu().numpy()
            )
        if (y_true_cls == 1).any():
            metrics['angular_error_t'] = calc_angular_error(
                y_pred_rgb[y_true_cls == 1].detach().cpu().numpy(),
                y_true_rgb[y_true_cls == 1].detach().cpu().numpy()
            )

        return loss, metrics
