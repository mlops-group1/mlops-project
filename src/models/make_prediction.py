import os
import click
import json
import pandas as pd

from statistics import angular_error, calc_statistics


@click.command()
@click.argument('gt_path', type=click.Path(exists=True))
@click.argument('preds_path', type=click.Path(exists=True))
def make_prediction(gt_path: str, preds_path: str) -> None:
    """
    Computes cosine metrics between ground truth labels and model predictions.
    Args:
        gt_path (str): Path to the ground truth labels .csv file.
        preds_path (str): Path to the model predictions .csv file.
    """

    gt = pd.read_csv(gt_path)
    preds = pd.read_csv(preds_path)

    # Set the image column as the index
    gt.set_index('image', inplace=True)
    preds.set_index('image', inplace=True)

    # Sort the data frame to align the rows
    gt.sort_index(inplace=True)
    preds.sort_index(inplace=True)

    # Calculate statistics
    angular_errors = angular_error(gt.values, preds.values)
    metrics = calc_statistics(angular_errors)

    # Save the metrics to a JSON file
    metrics_dir_path = 'models/'
    metrics_filename = 'metrics.json'
    metrics_path = os.path.join(metrics_dir_path, metrics_filename)

    # Load existing results
    if os.path.isfile(metrics_path):
        with open(metrics_path, 'r') as f:
            existing_metrics = json.load(f)
    else:
        existing_metrics = {}

    # Append new results.
    key = os.path.dirname(preds_path).split('/')[-1] + '_' \
        + os.path.basename(preds_path).split('.csv')[0]
    existing_metrics[key] = metrics

    # Save updated results
    with open(metrics_path, 'w') as f:
        json.dump(existing_metrics, f, indent=4)


if __name__ == '__main__':
    make_prediction()
