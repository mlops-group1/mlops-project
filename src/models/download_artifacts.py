import os
import click
import wandb


@click.command()
@click.argument('wandb_run_path', type=click.STRING)
@click.argument('output_dir_path', type=click.STRING)
def download_artifacts(wandb_run_path: str, output_dir_path: str) -> None:
    """
    Download the weights of a specific Weights & Biases run.
    Args:
        wandb_run_path (str): The path to the wandb run in the format
            "username/project-name/artifact-name:version".
        output_dir_path (str): The directory where the configuration will be saved.
    """

    run = wandb.init()
    run_name = wandb_run_path.split('/')[-1]

    artifact = run.use_artifact(wandb_run_path, type='model')
    artifact.download(os.path.join(output_dir_path, run_name))

    print(f"Weights for run {wandb_run_path} downloaded to {output_dir_path}")


if __name__ == '__main__':
    download_artifacts()
