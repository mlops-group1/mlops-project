import os
import glob
import pandas as pd

from tqdm import tqdm
from PIL import Image


def preprocess_images(
    src_folder: str,
    dst_folder: str,
    img_size: tuple[int, int] = (648, 432)
) -> None:
    """
    This function preprocesses images for TCC dataset and saves the processed images
    and their corresponding ground truth values in the destination directory.

    Args:
        src_folder (str): The source directory containing the raw data.
        dst_folder (str): The destination directory where for the processed data.
        img_size (tuple): The desired size of the output images.
    """

    # Create destination folders if not exist
    os.makedirs(dst_folder, exist_ok=True)
    os.makedirs(os.path.join(dst_folder, 'PNG'), exist_ok=True)

    # Create a dataframe with image name and r, g, b of a source estimation
    gt_df = pd.DataFrame(columns=['image', 'mean_r', 'mean_g', 'mean_b'])

    for folder in tqdm(glob.glob(os.path.join(src_folder, '*'))):
        with open(os.path.join(folder, 'groundtruth.txt'), 'r') as f:
            groundtruth = f.read().strip().split(',')
            mean_r, mean_g, mean_b = groundtruth

        for img_file in glob.glob(os.path.join(folder, '*.png')):
            # Skip the groundtruth.png and mask.png images
            if 'groundtruth' in img_file or 'mask' in img_file:
                continue

            img = Image.open(img_file)
            img = img.resize(img_size, Image.ANTIALIAS)

            folder_name = os.path.dirname(img_file).split('/')[-1]

            new_img_file = os.path.join(
                dst_folder, 'PNG', folder_name + '_' + os.path.basename(img_file)
            )
            img.save(new_img_file)

            img_name = os.path.basename(new_img_file).split('.')[0]
            gt_df = gt_df.append({
                'image': img_name,
                'mean_r': mean_r,
                'mean_g': mean_g,
                'mean_b': mean_b
            }, ignore_index=True)

    gt_df.to_csv(os.path.join(dst_folder, 'gt.csv'), index=False)


if __name__ == '__main__':
    src_folder = 'data/raw/TCC/training'
    dst_folder = 'data/processed/TCC/train'
    preprocess_images(src_folder, dst_folder)

    src_folder = 'data/raw/TCC/test'
    dst_folder = 'data/processed/TCC/test'
    preprocess_images(src_folder, dst_folder)
