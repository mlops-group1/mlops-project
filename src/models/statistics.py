import numpy as np


def calc_statistics(angular_errors: np.array) -> dict:
    avg = np.mean(angular_errors)
    p25 = np.percentile(angular_errors, q=25)
    p50 = np.percentile(angular_errors, q=50)
    p75 = np.percentile(angular_errors, q=75)
    avg_10worst = np.mean(np.sort(angular_errors)[-10:])
    return dict(avg=avg, p25=p25, p50=p50, p75=p75, avg_10worst=avg_10worst)


def angular_error(gt: np.array, preds: np.array) -> np.array:
    preds_norm = preds / np.linalg.norm(preds, axis=1, keepdims=True)
    gt_norm = gt / np.linalg.norm(gt, axis=1, keepdims=True)
    scalar_product = np.clip(np.sum(preds_norm * gt_norm, axis=1), -1.0, 1.0)
    angular_errors = np.degrees(np.arccos(scalar_product))
    return angular_errors
