import torch
import torch.optim.lr_scheduler as lr
from torch.utils.data import DataLoader, random_split
from torchvision import transforms

import os
import click
import wandb
import yaml
import numpy as np
from yaml.loader import FullLoader
from functools import partial

from src.data.dataset import ColorConstancyDataset, DADataset
from src.models.nets import FullColorCerberusDA
from src.models.trainer import Trainer
from src.models.losses import DALoss

from albumentations import Compose, HorizontalFlip, VerticalFlip, Resize
from albumentations.pytorch import ToTensorV2


GENERATOR = torch.Generator()


class Augmentation:
    def __init__(self, transforms):
        self.transforms = Compose(transforms)

    def __call__(self, sample):
        image_name = sample['image_name']
        image = sample['image']
        light_source = sample['light_source']

        image = self.transforms(image=image)['image']
        return {
            'image_name': image_name,
            'image': image,
            'light_source': light_source
        }


class model(FullColorCerberusDA):
    def __init__(self, *args, device='cpu', **kwargs):
        super().__init__(*args, **kwargs)
        self.device = device


def fix_seed():
    SEED = 42
    GENERATOR.manual_seed(SEED)
    torch.manual_seed(SEED)
    np.random.seed(SEED)
    torch.cuda.manual_seed(SEED)
    torch.cuda.manual_seed_all(SEED)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = True


def make_dataset(
    s_dir,
    t_dir,
    batch_size,
    t_labeled_size,
    ratio,
    num_workers=8,
    augment=False
):

    if augment:
        transform_list = [
            Augmentation([
                Resize(128, 192),
                HorizontalFlip(),
                VerticalFlip(),
                ToTensorV2()
            ])
        ]
    else:
        transform_list = [
            Augmentation([Resize(128, 192), ToTensorV2()])
        ]

    dataset_s = ColorConstancyDataset(
        csv_file=os.path.join(s_dir, 'gt.csv'),
        root_dir=os.path.join(s_dir, 'PNG'),
        transform=transforms.Compose(transform_list)
    )

    dataset_t = ColorConstancyDataset(
        csv_file=os.path.join(t_dir, 'gt.csv'),
        root_dir=os.path.join(t_dir, 'PNG'),
        transform=transforms.Compose(transform_list)
    )

    if t_labeled_size != 0:
        dataset_t_labeled, dataset_t_unlabeled = random_split(
            dataset_t,
            [t_labeled_size, len(dataset_t) - t_labeled_size],
            generator=GENERATOR,
        )
    else:
        dataset_t_labeled = dataset_t
        dataset_t_unlabeled = None

    da_dataset = DADataset(
        src_dataset=dataset_s,
        trg_dataset=dataset_t_labeled,
        trg_dataset_unlabeled=dataset_t_unlabeled,
        ratio=ratio
    )

    return DataLoader(
        dataset=da_dataset,
        batch_size=batch_size,
        shuffle=True,
        num_workers=num_workers,
        drop_last=False
    )


@click.command()
@click.argument('config_path', type=click.Path(exists=True))
def main(config_path: str):
    fix_seed()

    with open(config_path, 'r') as stream:
        config = yaml.load(stream, Loader=FullLoader)

    dl_test = make_dataset(
        s_dir=os.path.join(config['s_dir'], 'test'),
        t_dir=os.path.join(config['t_dir'], 'test'),
        t_labeled_size=0,
        ratio=[1, 0],
        batch_size=config['other_params']['batch_size']
    )
    dl_train = make_dataset(
        s_dir=os.path.join(config['s_dir'], 'train'),
        t_dir=os.path.join(config['t_dir'], 'train'),
        t_labeled_size=config['other_params']['t_labeled_size'],
        ratio=config['other_params']['ratio'],
        batch_size=config['other_params']['batch_size'],
        augment=True
    )

    loss = DALoss(domain_ratio=config['other_params']['domain_ratio'])
    model_factory = partial(model)
    optimizer_factory = partial(torch.optim.AdamW)
    scheduler_factory = partial(lr.ExponentialLR)

    model_params = config['model_params']
    optimizer_params = config['optimizer_params']
    scheduler_params = config['scheduler_params']
    learning_params = config['learning_params']
    other_params = config['other_params']
    forward_params = config['forward_params']
    wandb_init_params = config['wandb_init_params']

    trainer = Trainer(
        dl_train, dl_test,
        loss,
        model_factory=model_factory,
        optimizer_factory=optimizer_factory,
        scheduler_factory=scheduler_factory,
        model_params=model_params,
        optimizer_params=optimizer_params,
        scheduler_params=scheduler_params,
        other_params=other_params,
        log=True,
        wandb_init_params=wandb_init_params,
        forward_params=forward_params,
    )
    trainer.train_model(learning_params)
    wandb.finish()


if __name__ == '__main__':
    main()
