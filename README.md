# Сolor constancy domain adaptation algorithm

This repository contains the implementation of a lighting estimation algorithm that is independent of the digital camera (domain) used to capture a given photograph.

## Overview

Traditional digital cameras rely on statistical methods for scene parameter estimation. These methods, however, exhibit significantly lower accuracy compared to machine learning techniques. The reasons for this are twofold:

1. Artificial intelligence-based methods require a large labeled training dataset, the annotation of which demands a substantial amount of time and precision from color engineers.
2. The limited generalization capability of these methods when applied to new cameras, given that all cameras possess varying spectral characteristics.

The goal of this project is to develop a lighting estimation algorithm that addresses these issues.

## Downloading Model Weights

We provide a script `download_artifacts.py` to download the weights of a specific Weights & Biases run. The script expects the following arguments:

- `wandb_run_path`: The path to the Weights & Biases run from which you want to download the model weights. The path should be in the format `username/project-name/artifact-name:version`.
- `output_dir_path`: The directory where the downloaded weights will be saved.

You can run the `download_artifacts.py` script from the command line as follows:

```sh
python download_artifacts.py [wandb_run_path] [output_dir_path]
```

For example, to download weights from a Weights & Biases run located at `user123/projectX/model-v1` and save them in a directory located at `models/weights`, you would run:

```sh
python download_artifacts.py user123/projectX/model-v1 models/weights
```

## Model Evaluation

We provide a script `evaluate.py` to evaluate a pre-trained model on a dataset and save the results. The script expects the following arguments:

- `model_weights_path`: The path to the file containing the model weights.
- `output_path`: The directory where the predictions will be saved.
- `architecture`: The type of the encoder part of the model. Current options are `color_cerberus` and `vit`.
- `dataset_path`: The directory where the images to be evaluated are stored. This directory should also contain a .csv file with the names of the images.

You can run the `evaluate.py` script from the command line as follows:

```sh
python evaluate.py [model_weights_path] [output_path] [architecture] [dataset_path]
```

For example, to evaluate a `color_cerberus` model with weights stored at `models/model_weights.pth` on a dataset located at `data/my_dataset`, and save the predictions in `models/predictions`, you would run:

```sh
python evaluate.py models/model_weights.pth models/predictions color_cerberus data/my_dataset
```

The script will save the model's predictions as a .csv file in the specified output directory.


## Project Organization


    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>

